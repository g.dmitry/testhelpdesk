import io.qameta.allure.Attachment;
import io.qameta.allure.Step;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import pages.*;
import ru.yandex.qatools.ashot.AShot;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Random;
import java.util.concurrent.TimeUnit;

public class HelpdeskUITest {





    static String file = "C:\\Users\\Demon\\Desktop\\lightShot\\helpdesk.png";
    static String mail = "mail@mail.ru";
    static int id = new Random().nextInt(500000); // просто нужно создать произвольный айди
    static String problem = "problem" + Integer.toString(id);
    static String date = "2021-10-05 00:00:00";
    final String SITE_URL = "https://at-sandbox.workbench.lanit.ru/";
    String problemDescription = Integer.toString(id) + "problem";
    private WebDriver driver;

    @Before
    public void setup() throws IOException {

        System.getProperties().load(ClassLoader.getSystemResourceAsStream("config.properties"));
        System.setProperty("webdriver.chrome.driver", "F:\\testQA\\chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        AbstractPage.setDriver(driver);
        driver.get("https://at-sandbox.workbench.lanit.ru/");

    }

    @Test

    public void createTicketTest() {
        String priotry = "2. High";
        WebElement newTicket = driver.findElement(By.xpath("//span[contains(text(),'New Ticket')]"));
        newTicket.click();
        CreateTicketsPage createTicket = new CreateTicketsPage();
        createTicket.setInputQueue("2");
        createTicket.setInputProblem(problem);
        createTicket.setInputProblemDescription(problemDescription);
        createTicket.setInputPriority(priotry);
        createTicket.setInputDate(date);
        createTicket.setInputFile(file);
        createTicket.setInputMail(mail);
        createTicket.setCreateTicket();
        MainPage mainPage = new MainPage();
        mainPage.setLogIn();
        LoginPage loginPage = new LoginPage();
        try {
            System.getProperties().load(ClassLoader.getSystemResourceAsStream("user.properties"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        loginPage.login(System.getProperty("user"), System.getProperty("password"));
        loginPage.setInputLogin();
        SearchPage searchPage = new SearchPage();
        searchPage.inputSearch(problem);
        searchPage.inputSearchGo();
        WebElement getTicket = driver.findElement(By.xpath("//*[contains(text(),'" + problem + "')]"));
        getTicket.click();
        TicketPage ticketPage = new TicketPage();
        Assert.assertTrue("Значение не совпадает!", ticketPage.checkPriority(priotry));
        Assert.assertTrue("Почта не совпадает", ticketPage.checkMail(mail));



    }
//       @After
//       public void close(){
//           driver.close();
//       }


}
