package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class TicketPage extends AbstractPage {


    WebElement priority = driver.findElement(By.xpath("//td[@class='table-warning']"));

    public boolean checkPriority(String expected) {
        return priority.getText().equals(expected);

    }

    public boolean checkMail(String expected) {
        String xpath = "//td[contains(text(),'" + expected + "')]";
        if (driver.findElements(By.xpath(xpath)).size() > 0) {
            return true;
        } else {
            return false;
        }

    }
}
