package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class LoginPage extends AbstractPage {
    @FindBy(xpath = "//input[@class='btn btn-lg btn-primary btn-block']")
    WebElement inputLogin;
    @FindBy(id = "username")
    private WebElement user;
    @FindBy(id = "password")
    private WebElement password;
    public LoginPage() {
        PageFactory.initElements(driver, this);
    }

    public void login(String user, String password) {
        this.user.sendKeys(user);
        this.password.sendKeys(password);


    }

    public void setInputLogin() {
        inputLogin.click();
    }
}
