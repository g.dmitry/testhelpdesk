package pages;

import io.qameta.allure.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

public class CreateTicketsPage extends AbstractPage {


    private Select newTicketQueue = new Select(driver.findElement(By.xpath("//select[@name='queue']")));
    private WebElement inputProblem = driver.findElement(By.xpath("//input[@name='title']"));
    private WebElement inputProblemDescription = driver.findElement(By.xpath("//textarea[@name='body']"));
    private Select inputPriority = new Select(driver.findElement(By.xpath("//select[@name='priority']")));

    private WebElement inputDate = driver.findElement(By.xpath("//input[@name='due_date']"));

    private WebElement inputFile = driver.findElement(By.xpath("//input[@type='file']"));
    private WebElement inputMail = driver.findElement(By.xpath("//input[@id='id_submitter_email']"));
    private WebElement creatTicket = driver.findElement(By.xpath("//button[normalize-space()='Submit Ticket']"));

    @Step("Название билета")
    public void setInputProblem(String text) {
        inputProblem.sendKeys(text);
        screenshot();

    }
    @Step("Очередь")
    public void setInputQueue(String text) {
        newTicketQueue.selectByValue(text);
        screenshot();

    }
    @Step("Описание проблемы")
    public void setInputProblemDescription(String text) {
        inputProblemDescription.sendKeys(text);
        screenshot();

    }
    @Step("Выбор приоритета")
    public void setInputPriority(String text) {
        inputPriority.selectByVisibleText(text);
        screenshot();
    }
    @Step("Выбор даты")
    public void setInputDate(String text) {
        inputDate.sendKeys(text);
        screenshot();
    }
    @Step("Загрузка файла")
    public void setInputFile(String text) {
        inputFile.sendKeys(text);
        screenshot();
    }
    @Step("Загрузка файла")
    public void setInputMail(String text) {
        inputMail.sendKeys(text);
        screenshot();
    }
    @Step("Создание билета")
    public void setCreateTicket() {
        creatTicket.click();
        screenshot();
    }
}
