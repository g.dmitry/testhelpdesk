package pages;

import io.qameta.allure.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class SearchPage extends AbstractPage {

    WebElement search = driver.findElement(By.xpath("//input[@class='form-control']"));
    WebElement searchGo = driver.findElement(By.xpath("//button[@class='btn btn-primary']"));

    @Step("Вводим название билета")
    public void inputSearch(String text) {
        search.sendKeys(text);
        screenshot();
    }

    @Step("Просмотр билетов")
    public void inputSearchGo() {
        searchGo.click();
        screenshot();
    }


}