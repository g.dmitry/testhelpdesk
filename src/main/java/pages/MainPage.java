package pages;

import io.qameta.allure.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class MainPage extends AbstractPage {

    WebElement newTicket = driver.findElement(By.xpath("//span[contains(text(),'New Ticket')]"));
    WebElement logIn = driver.findElement(By.xpath("//a[@id='userDropdown']"));


    public void setNewTicket() {
        newTicket.click();
    }
    @Step("Логинемся")
    public void setLogIn() {
        logIn.click();
        screenshot();
    }

    public void setSearch(String text) {

    }


}
